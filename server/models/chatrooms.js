'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class chatRooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  chatRooms.init({
    id: DataTypes.UUID,
    userIds: DataTypes.ARRAY(DataTypes.UUID),
    type: DataTypes.STRING,
    chatInitiator: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'chatRooms',
  });
  return chatRooms;
};