const express = require("express");
// controllers
const user = require("../controllers/user.js");

const router = express.Router();

router
  .post("/", user.onCreateUser)
  .get("/:id", user.onGetUserById)
  .get("/", user.onGetAllUsers)
  .delete("/:id", user.onDeleteUserById);

module.exports = router;
