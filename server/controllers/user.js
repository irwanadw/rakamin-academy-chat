const bcrypt = require("bcrypt");

// utils
const makeValidation = require("@withvoid/make-validation");
// models
// enums
const typeEnum = require("../enums/types");
const uuid = require("uuid");
const db = require("../models");

module.exports = {
  onCreateUser: async (req, res) => {
    try {
      const salt = await bcrypt.genSalt(10);

      const validation = makeValidation((types) => ({
        payload: req.body,
        checks: {
          userName: { type: types.string },
          firstName: { type: types.string },
          lastName: { type: types.string },
          type: { type: types.string, options: { enum: typeEnum } },
        },
      }));
      if (!validation.success) return res.status(400).json(validation);

      const { userName, password, firstName, lastName, type } = req.body;
      const hashPassword = await bcrypt.hash(password, salt);
      const payload = {
        id: uuid.v4(),
        userName,
        password: hashPassword,
        firstName,
        lastName,
        type,
      };
      const user = await db.users.create(payload);

      return res.status(200).json({ success: true, user });
    } catch (error) {
      return res.status(500).json({ success: false, error: error.message });
    }
  },
  onGetUserById: async (req, res) => {
    try {
      const user = await db.users.findOne({ where: { id: req.params.id } });
      console.log(user);
      if (!user) throw { error: "No user with this id found" };
      return res.status(200).json({ success: true, user });
    } catch (error) {
      return res.status(500).json({ success: false, error: error });
    }
  },
  onGetAllUsers: async (req, res) => {
    try {
        const users = await db.users.findAll();
        return res.status(200).json({ success: true, users });
      } catch (error) {
        return res.status(500).json({ success: false, error: error })
      }
  },
  onDeleteUserById: async (req, res) => {},
};
