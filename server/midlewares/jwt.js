const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const SECRET_KEY = "Rakamin_Academy";
const db = require("../models");

const encode = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const user = await db.users.findOne({
      where: {
        id: userId,
      },
      raw: true,
    });
    !user &&
      res.status(404).json({
        status: "login failed",
        message: `Users with username ${req.body.userName} not registered`,
      });
    const isPasswordMatch = await bcrypt.compare(
      req.body.password,
      user.password
    );
    console.log(isPasswordMatch)

    const accesToken = jwt.sign(
      {
        id: user.id,
        type: user.type,
      },
      SECRET_KEY,
      { expiresIn: "1d" }
    );

    const { password, ...others } = user;

    isPasswordMatch == true
      ? res.status(200).json({ ...others, status: "success", accesToken })
      : res.status(401).json({
          status: "login failed",
          message: "Wrong password",
        });
    req.authToken = accesToken;
    next();
  } catch (error) {
    return res.status(400).json({ success: false, message: error.error });
  }
};

const decode = async (req, res, next) => {
  if (!req.headers["authorization"]) {
    return res
      .status(400)
      .json({ success: false, message: "No access token provided" });
  }
  const accessToken = req.headers.authorization.split(" ")[1];
  try {
    const decoded = jwt.verify(accessToken, SECRET_KEY);
    req.userId = decoded.userId;
    req.userType = decoded.type;
    return next();
  } catch (error) {
    return res.status(401).json({ success: false, message: error.message });
  }
};

module.exports = { decode, encode };
